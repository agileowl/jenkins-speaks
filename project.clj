(defproject jenkins-speaks "0.1-SNAPSHOT"
  :description "Jenkins speaks!"
  :url "https://bitbucket.org/agileowl/jenkins-speaks"
  :dependencies [[org.clojure/clojure "1.2.0"]
                 [org.clojure/clojure-contrib "1.2.0"]
                 [org.clojure/data.json "0.1.2"]
                 [commons-io/commons-io "2.1"]]
  :aot [jenkins-speaks.core]
  :main jenkins-speaks.core)

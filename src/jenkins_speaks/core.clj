(ns jenkins-speaks.core
  (use [clojure.string :only (replace-first)]
       [clojure.data.json :only (read-json json-str)]
       [clojure.contrib.shell-out :only (sh)]))

(import '(java.io InputStreamReader BufferedReader)
        '(java.net Socket ServerSocket SocketException)
        '(org.apache.commons.io IOUtils))

(defn on-thread [f]
  (doto (new Thread f) (.start)))

(defn create-server 
  "creates and returns a server socket on port, will pass the client socket to accept-socket on connection" 
  [accept-socket port]
  (let [ss (new ServerSocket port)]
    (on-thread #(when-not (. ss (isClosed))
      (try (accept-socket (. ss (accept)))
      (catch SocketException e))
      (recur)))
ss))

(defn speak!
  "Jenkins speaks!"
  [message]
  (sh "/usr/bin/festival" "--tts" :in message))

(defn jenkins-speaks!
  "Jenkins speaks!"
  [is]
  (let [response (IOUtils/toString is "UTF-8")]
    (let [json (read-json (subs response (. response (indexOf "{"))))]
      (println json)
      (let [build (:build json)]
        (if (= "COMPLETED" (:phase build))
          (speak! (format "%s build %d is %s" (replace-first (:name json) #"_" " ") (:number build) (:status build))))))))

(defn speak-on-accept-socket
  "handles the build notification from jenkins"
  [s] (on-thread #(jenkins-speaks! (. s (getInputStream)))))

(def server (create-server speak-on-accept-socket 6060))
